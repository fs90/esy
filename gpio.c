#include <linux/module.h>
#include <linux/fs.h>
#include <asm/io.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
static dev_t gpio_dev_number;
static struct cdev *driver_object;
static struct class *gpio_class;
static struct device *motorl_dev, *motorr_dev;
struct instance_data {
 int gpio_1, gpio_2;
};


#define ML1 23
#define ML2 24 
#define MR1 8
#define MR2 25 


int configure_gpio(int number)
{

 char buf[128];
 snprintf(buf, sizeof(buf) / sizeof(char), "gpio%d", number);
 printk("configure %s\n", buf);
 int err = gpio_request(number, buf);
 if(err) {
  printk("gpio_request %d: %d\n", number, err);
  return -1;
 }
 err = gpio_direction_output(number, 0);
 if(err) {
  printk("gpio_direction %d: %d\n", number, err);
  return -1;
 }

 return 0;
}

// TODO: ML1, ML2, MR1 und MR2 definieren.
static int driver_open( struct inode *geraetedatei, struct file *instanz )
{
 struct instance_data *id;
 id = kmalloc( sizeof(struct instance_data), GFP_USER );
 if (id==NULL) {
  printk("kmalloc failed\n");
  return -EIO;
 }
 instanz->private_data = (struct instance_data *) id;
 if ( iminor(geraetedatei)==0 ) { // motor-left
  id->gpio_1 = ML1;
  id->gpio_2 = ML2;
 } else { // motor-right
  id->gpio_1 = MR1;
  id->gpio_2 = MR2;
 }

 int err = configure_gpio(id->gpio_1);
 if(err == -1) {
  return -1;
 }

 err = configure_gpio(id->gpio_2);
 if(err == -1) {
  return -1;
 }

 // TODO: Initialisieren der GPIOs
 printk("gpio %d and  %d successfull configured\n",
  id->gpio_1, id->gpio_2);
 return 0;
}
static int driver_close( struct inode *geraete_datei, struct file *instanz )
{
 struct instance_data *id=(struct instance_data *)instanz->private_data;


 dev_info( motorl_dev, "driver_close called\n");
 if  (id!=NULL) {
  gpio_free( id->gpio_1 );
  gpio_free( id->gpio_2 );
  kfree( id );
 }
 return 0;
}
static ssize_t driver_write( struct file *instanz, const char __user *user,
  size_t count, loff_t *offset )
{

 char *buffer = kmalloc(sizeof(char) * count, 0);
 unsigned long not_copied = 0, to_copy = count;

 to_copy = count;
 not_copied = copy_from_user(buffer, user, to_copy);
 int value=0;

 struct instance_data *id=(struct instance_data *)instanz->private_data;
 // TODO: Zugriff auf die Hardware implementieren

 if(strstr(buffer, "-1") != NULL) {
  gpio_set_value(id->gpio_1, 1);
  gpio_set_value(id->gpio_2, 0);
  printk("gpio: direction: backwards\n");
 }
 else if(strstr(buffer, "1") != NULL) {
  gpio_set_value(id->gpio_1, 0);
  gpio_set_value(id->gpio_2, 1);
  printk("gpio: direction: forwards\n");
 }
 else if(strstr(buffer, "0") != NULL) {
  gpio_set_value(id->gpio_1, 0);
  gpio_set_value(id->gpio_2, 0);
  printk("gpio: brake\n");
 }

 kfree(buffer);
 return to_copy-not_copied;
}
static struct file_operations fops = {
 .owner= THIS_MODULE,
 .write= driver_write,
 .open= driver_open, 
 .release= driver_close,
};
static int __init mod_init( void )
{
 if( alloc_chrdev_region(&gpio_dev_number,0,2,"motor")<0 )
  return -EIO;
 driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
 if( driver_object==NULL )
  goto free_device_number;
 driver_object->owner = THIS_MODULE;
 driver_object->ops = &fops;
 if( cdev_add(driver_object,gpio_dev_number,2) )
  goto free_cdev;
 /* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
 gpio_class = class_create( THIS_MODULE, "motor" );
 if( IS_ERR( gpio_class ) ) {
  pr_err( "gpio: no udev support\n");
  goto free_cdev;
 }
 motorl_dev = device_create( gpio_class, NULL, gpio_dev_number,
  NULL, "%s", "motor-left" );
 motorr_dev = device_create( gpio_class, NULL, gpio_dev_number+1,
  NULL, "%s", "motor-right" );
 dev_info(motorl_dev, "mod_init");

 

 return 0;
free_cdev:
 kobject_put( &driver_object->kobj );
free_device_number:
 unregister_chrdev_region( gpio_dev_number, 2 );
 return -EIO;
}
static void __exit mod_exit( void )
{
 dev_info(motorl_dev, "mod_exit");
 /* Loeschen des Sysfs-Eintrags und damit der Geraetedatei */
 device_destroy( gpio_class, gpio_dev_number+1 );
 device_destroy( gpio_class, gpio_dev_number );
 class_destroy( gpio_class );
 /* Abmelden des Treibers */
 cdev_del( driver_object );
 unregister_chrdev_region( gpio_dev_number, 2 );
 return;
}
module_init( mod_init );
module_exit( mod_exit );
/* Metainformation */
MODULE_LICENSE("GPL");


