/**
 * @file   movementsensor.c
 * @author Florian Scholz
 * @date   21. June 2017
 * @brief  A kernel module for measuring distance with a ultrasonic distance
 * @see http://www.derekmolloy.ie/
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>                 // Required for the GPIO functions
#include <linux/interrupt.h>            // Required for the IRQ code
#include <linux/wait.h>
#include <linux/spinlock.h>
#include <linux/ktime.h>
#include <linux/delay.h>
#include <asm/uaccess.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Florian Scholz");
MODULE_DESCRIPTION("A driver for the distance measuring using ultrasonic sensors");
MODULE_VERSION("0.1");

#define GPIO_HIGH 1
#define DEVICE_NAME "ultrasonicfs"
#define CLASS_NAME "mvsfs"

enum Device_State { NotInitialized, Ready, Started, Stopped };

static unsigned int gpioEchoPort = 17;       //The 'Echo' gpio pin
static unsigned int gpioTriggerPort = 4; // The 'Trigger' gpio pin

static unsigned int irqNumber;          ///< Used to share the IRQ number within this file
static int majorNumber;
static ktime_t measurement_started;
static ktime_t measurement_stopped;

static irq_handler_t  ulsgpio_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);
static struct class*  ulscharClass  = NULL; ///< The device-driver class struct pointer
static struct device* ulscharDevice = NULL; ///< The device-driver device struct pointer
static wait_queue_head_t wq;

static enum Device_State deviceState = NotInitialized;

static spinlock_t time_lock;
static int ulsgpio_open(struct inode *inodep, struct file *filep){
   return 0;
}

#if 0
static void print_ktime(const char* info, ktime_t sample) {
  struct timespec ts_last = ktime_to_timespec(sample);
  printk("%s: %ld\n",info, ts_last.tv_nsec);
}
#endif

static void ulsgpio_send_trigger(void) {
	gpio_set_value(gpioTriggerPort, 1); 
	deviceState = Ready;
	usleep_range(10,11);
	gpio_set_value(gpioTriggerPort, 0);
}

static ssize_t ulsgpio_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
	char pbuf[256];
	struct timespec ts_last;
	int cnt;

	ulsgpio_send_trigger();

	wait_event_interruptible(wq, deviceState == NotInitialized);

	spin_lock_irq(&time_lock);
	ts_last = ktime_to_timespec(ktime_sub(measurement_stopped, measurement_started));
	spin_unlock_irq(&time_lock);

	cnt = snprintf(pbuf, min((size_t)256,len), "%ld\n", ts_last.tv_nsec);
	len = cnt - copy_to_user(buffer, pbuf, cnt);
	return len;
}

static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = ulsgpio_open,
	.read = ulsgpio_read
};

static int __init ulsgpio_init(void){
	int result = 0;
	measurement_started = measurement_stopped = ktime_set(0,0);
	spin_lock_init(&time_lock);
	printk(KERN_INFO "GPIO_TEST: Initializing the GPIO_TEST LKM\n");

	if (!gpio_is_valid(gpioEchoPort)){
		printk(KERN_INFO "GPIO_TEST: invalid LED GPIO\n");
		return -ENODEV;
	}
	
	majorNumber = register_chrdev(0, DEVICE_NAME, &fops);

	if (majorNumber<0){
		printk(KERN_ALERT "ULTRASONIC: failed to register a major number\n");
		return majorNumber;
	}

	ulscharClass = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(ulscharClass)) {
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to register device class\n");
		return PTR_ERR(ulscharClass);
	}
	
	ulscharDevice = device_create(ulscharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(ulscharDevice)) {
		class_destroy(ulscharClass);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "Failed to create the device\n");
		return PTR_ERR(ulscharDevice);
	}

	gpio_request(gpioEchoPort, "sysfs");
	gpio_direction_input(gpioEchoPort);
	gpio_export(gpioEchoPort, false);

	gpio_request(gpioTriggerPort, "sysfs");
	gpio_direction_output(gpioTriggerPort, 0);
	gpio_export(gpioTriggerPort, false);


	init_waitqueue_head(&wq);


	irqNumber = gpio_to_irq(gpioEchoPort);

	result = request_irq(irqNumber,
	(irq_handler_t) ulsgpio_irq_handler,
	IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
	"uls_gpio_handler",
	NULL);

	return result;
}


static void ulsgpio_release_gpio_port(unsigned int gpioPort) {
	gpio_unexport(gpioPort);
	gpio_free(gpioPort);;
}

static void __exit ulsgpio_exit(void) {
	 /* Removing the IRQ routine */
	free_irq(irqNumber, 0);
	
	ulsgpio_release_gpio_port(gpioEchoPort);
	ulsgpio_release_gpio_port(gpioTriggerPort);
	
	/* Remove udev stuff */
	device_destroy(ulscharClass, MKDEV(majorNumber, 0));
	class_unregister(ulscharClass);
	class_destroy(ulscharClass);

	/* Remove character device */
	unregister_chrdev(majorNumber, DEVICE_NAME);
}

static irq_handler_t ulsgpio_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs){
	int gpio_number = gpioEchoPort;
	if (gpio_number == gpioEchoPort && deviceState == Ready)
	{
		int value = gpio_get_value(gpioEchoPort);
                if(value == GPIO_HIGH)
                {
                        
			measurement_started = ktime_get();
			deviceState = Ready;
                	return (irq_handler_t) IRQ_HANDLED;
                }
                else
                {
			measurement_stopped = ktime_get();
			deviceState = Ready;

			spin_lock(&time_lock);
			wake_up_interruptible(&wq);
			spin_unlock(&time_lock);
                }
	}
	return (irq_handler_t) IRQ_HANDLED;
}

module_init(ulsgpio_init);
module_exit(ulsgpio_exit);
